# README #

###Instalando o Android Studio de um jeito facil no linux###

Basta digitar isso no terminal


```
#!bash

sudo apt-add-repository ppa:paolorotolo/android-studio
sudo apt-get update
sudo apt-get install android-studio
```


###Como configurar o Android Studio###

Para configurar o projeto, siga esses passos:

* Instalar o Android Studio na configuracao Standart

* Na tela de escolha de SDK, escolha uma maior que 4.1

* Importar o projeto "Almanaque Rio 2016"

    **IMPORTANTE:** Não criar um projeto do zero, clique em open e va ate a pasta onde esta o repositorio

* Instalar as bibliotecas e SDKs que ele sugerir

* Na primeira vez que executar o projeto, ele vai pedir pra vc criar um aparelho simulado. Quando for criar utilize uma versao do Android **com Google APIs**, pra ele poder carregar o maps

### Git ###

* Para iniciar o repositorio e baixar pela primeira vez, va até a pasta destino pelo terminal e use:

```
#!bash
 git clone https://user@bitbucket.org/sabrina_beck/mc750.git

```

    e substitua user pelo seu usuario no bitbucket

* Para atualizar os arquivos locais na maquina com os do repositorio, use:

    
```
#!bash
git pull

```


* Para saber quais arquivos modificou, use:

    
```
#!bash
git status

```
  

* Para adicionar arquivos novos, use

    
    
```
#!bash
git add .

```
   

* Para criar um pacote de mudancas, use:


    
```
#!bash
git commit caminho_do_arquivo -m "mensagem"

```


    **IMPORTANTE:** Para saber o caminho do arquivo use git status antes

    **Dica**: Para commitar tudo, sem ter que colocar cada arquivo separadamente, utilize git commit -am "mensagem"

* Para descartar alguma mudanca local use:

    
```
#!bash
git checkout caminho_do_arquivo

```

* Para enviar as mudancas para o repositorio, use:
     
    
```
#!bash
git push

```