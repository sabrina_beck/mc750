package br.com.zephyros.almanaquerio2016.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;

import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import br.com.zephyros.almanaquerio2016.R;

/**
 * Created by sabrina on 21/11/15.
 */
public class DateFormatUtil {

    public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";

    public static String getReadableDateString(Activity activity, String dateAsString) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
        Date date = formatter.parse(dateAsString);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        int monthOfYear = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        return getReadableDateString(activity, year, monthOfYear, dayOfMonth, dayOfWeek);
    }

    public static String getReadableDateString(Activity activity, int year, int monthOfYear, int dayOfMonth, int dayOfWeek) {
        Context context = activity.getBaseContext();
        Resources resources = context.getResources();
        String dateFormat = resources.getString(R.string.date_format);
        String dayOfWeekName = resources.getString(resources.getIdentifier("day_of_week_" + dayOfWeek, "string", context.getPackageName()));
        String monthName = resources.getString(resources.getIdentifier("month_" + monthOfYear, "string", context.getPackageName()));
        return MessageFormat.format(dateFormat, dayOfWeekName, dayOfMonth, monthName, year + "");
    }

}
