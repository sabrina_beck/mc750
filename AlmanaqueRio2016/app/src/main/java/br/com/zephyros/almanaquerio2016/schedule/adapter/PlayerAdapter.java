package br.com.zephyros.almanaquerio2016.schedule.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.List;

import br.com.zephyros.almanaquerio2016.R;
import br.com.zephyros.almanaquerio2016.schedule.model.Player;

/**
 * Created by sabrina on 21/11/15.
 */
public class PlayerAdapter extends BaseAdapter {
    private final Context context;
    private final List<Player> players;

    public PlayerAdapter(Context context, List<Player> players) {
        this.context = context;
        this.players = players;
    }

    @Override
    public int getCount() {
        return players.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Player player = players.get(position);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View playerView = inflater.inflate(R.layout.game_schedule_player, null);

        TextView name = (TextView) playerView.findViewById(R.id.player_name);
        name.setText(player.getName());

        return playerView;
    }
}
