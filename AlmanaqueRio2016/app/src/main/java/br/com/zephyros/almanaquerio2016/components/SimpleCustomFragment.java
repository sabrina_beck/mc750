package br.com.zephyros.almanaquerio2016.components;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.zephyros.almanaquerio2016.R;

/**
 * Created by sabrina on 24/11/15.
 */
public class SimpleCustomFragment extends Fragment {

    public static final String FRAGMENT_LAYOUT = "fragmentLayout";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int layout = getArguments().getInt(FRAGMENT_LAYOUT);
        return inflater.inflate(layout, container, false);
    }
}
