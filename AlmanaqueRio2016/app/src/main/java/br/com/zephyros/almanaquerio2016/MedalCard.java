package br.com.zephyros.almanaquerio2016;

/**
 * Created by kaique on 08/11/15.
 */
public class MedalCard {

    public String position;
    public int flag;
    public String gold;
    public String silver;
    public String bronze;


    public MedalCard(String position, int flag, String gold, String silver, String bronze) {
        this.position = position;
        this.flag = flag;
        this.gold = gold;
        this.silver = silver;
        this.bronze = bronze;

    }
}
