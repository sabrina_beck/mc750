package br.com.zephyros.almanaquerio2016;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;

public class MedalActivity extends AppCompatActivity {

    private CardListAdapter cardArrayAdapter;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medal);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listView = (ListView) findViewById(R.id.card_listView);

        listView.addHeaderView(new View(this));
        listView.addFooterView(new View(this));

        cardArrayAdapter = new CardListAdapter(getApplicationContext(), R.layout.content_medal);

        cardArrayAdapter.add(new MedalCard("1º", R.drawable.us, "11", "2", "1"));
        cardArrayAdapter.add(new MedalCard("2º", R.drawable.ch, "10", "2", "1"));
        cardArrayAdapter.add(new MedalCard("3º", R.drawable.jp, "10", "2", "1"));
        cardArrayAdapter.add(new MedalCard("4º", R.drawable.cm, "9", "2", "1"));
        cardArrayAdapter.add(new MedalCard("5º", R.drawable.bd, "8", "2", "1"));
        cardArrayAdapter.add(new MedalCard("6º", R.drawable.es, "7", "2", "1"));
        cardArrayAdapter.add(new MedalCard("7º", R.drawable.fm, "6", "2", "1"));
        cardArrayAdapter.add(new MedalCard("8º", R.drawable.gh, "5", "2", "1"));
        cardArrayAdapter.add(new MedalCard("9º", R.drawable.hu, "4", "2", "1"));
        cardArrayAdapter.add(new MedalCard("11º", R.drawable.jo, "3", "2", "1"));

        listView.setAdapter(cardArrayAdapter);

    }

    public void startMedalDetail(View view) {
        Intent intent = new Intent(this, MedalDetailActivity.class);
        startActivity(intent);
    }
}
