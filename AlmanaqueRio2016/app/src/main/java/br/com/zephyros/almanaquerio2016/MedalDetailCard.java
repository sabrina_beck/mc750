package br.com.zephyros.almanaquerio2016;
public class MedalDetailCard {

    public String modality;
    public String date;
    public String place;
    public String athlete;


    public MedalDetailCard(String modality, String date, String place, String athlete) {
        this.modality = modality;
        this.date = date;
        this.place = place;
        this.athlete = athlete;
    }
}
