package br.com.zephyros.almanaquerio2016.schedule.model;

import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sabrina on 21/11/15.
 */
public class GameDetails {

    private String modality;
    private String date;
    private String time;
    private String brazilianChannel;
    private String place;
    private String competitionStage;
    private String Description;
    private String imageName;
    private List<Team> teams;

    public GameDetails(JSONObject details) throws JSONException {
        this.modality = details.getString("modality");
        this.date = details.getString("date");
        this.time = details.getString("time");
        this.brazilianChannel = details.getString("brazilianChannel");
        this.place = details.getString("place");
        this.competitionStage = details.getString("competitionStage");
        this.Description = details.getString("Description");
        this.imageName = details.getString("imageName");

        JSONArray teamsJson = details.getJSONArray("teams");
        this.teams = new ArrayList<>();
        for(int i = 0; i < teamsJson.length(); i++) {
            Team team = new Team(teamsJson.getJSONObject(i));
            this.teams.add(team);
        }
    }

    public String getModality() {
        return modality;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getBrazilianChannel() {
        return brazilianChannel;
    }

    public String getPlace() {
        return place;
    }

    public String getCompetitionStage() {
        return competitionStage;
    }

    public String getDescription() {
        return Description;
    }

    public String getImageName() {
        return imageName;
    }

    public List<Team> getTeams() {
        return teams;
    }
}
