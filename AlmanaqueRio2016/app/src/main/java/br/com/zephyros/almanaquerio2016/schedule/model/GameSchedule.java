package br.com.zephyros.almanaquerio2016.schedule.model;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.zephyros.almanaquerio2016.schedule.parser.JsonLoader;

/**
 * Created by sabrina on 20/11/15.
 */
public class GameSchedule {
    private static final String GAMES_SCHEDULE_FILE = "programacao/games_schedule.json";

    private String date;
    private String hour;
    private String modality;
    private String pictogram;
    private String detailsFile;

    public GameSchedule(JSONObject gameSchedule) throws JSONException {
        this.date = gameSchedule.getString("date");
        this.hour = gameSchedule.getString("time");
        this.modality = gameSchedule.getString("modality");
        this.pictogram = gameSchedule.getString("pictogram");
        this.detailsFile = gameSchedule.getString("details");
    }

    public static List<GameSchedule> getGamesSchedule(Context context) throws IOException {
        List<GameSchedule> gamesSchedule = new ArrayList<>();
        String json = JsonLoader.loadJSONFromAssets(GAMES_SCHEDULE_FILE, context);
        if(json != null) {
            try {
                JSONArray gamesScheduleJson = new JSONArray(json);
                for(int i = 0; i < gamesScheduleJson.length(); i++) {
                    GameSchedule gameSchedule = new GameSchedule(gamesScheduleJson.getJSONObject(i));
                    gamesSchedule.add(gameSchedule);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                return Collections.emptyList();
            }
        }
        return gamesSchedule;
    }

    public String getHour() {
        return hour;
    }

    public String getModality() {
        return modality;
    }

    public String getPictogram() {
        return pictogram;
    }

    public String getDate() {
        return date;
    }

    public String getDetailsFile() {
        return detailsFile;
    }
}
