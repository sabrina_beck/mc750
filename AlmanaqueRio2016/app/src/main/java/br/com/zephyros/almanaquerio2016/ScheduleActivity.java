package br.com.zephyros.almanaquerio2016;

import android.app.ActionBar;
import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import br.com.zephyros.almanaquerio2016.schedule.adapter.ScheduleTabsAdapter;
import br.com.zephyros.almanaquerio2016.schedule.components.ScheduleDatePickerFragment;

public class ScheduleActivity extends AppCompatActivity {

    private final ScheduleDatePickerFragment datePicker = new ScheduleDatePickerFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);

        datePicker.onParentActivityCreated(this);
    }

    public void showDatePickerDialog(View v) {
        datePicker.show(getFragmentManager(), "datePicker");
    }

}
