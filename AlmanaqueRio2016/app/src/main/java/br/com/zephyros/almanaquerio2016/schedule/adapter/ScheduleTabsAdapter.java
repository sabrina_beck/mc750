package br.com.zephyros.almanaquerio2016.schedule.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import br.com.zephyros.almanaquerio2016.R;
import br.com.zephyros.almanaquerio2016.components.SimpleCustomFragment;

/**
 * Created by sabrina on 24/11/15.
 */
public class ScheduleTabsAdapter extends FragmentPagerAdapter {
    public static final int NUMBER_OF_PAGES = 2;
    private static final int[] PAGE_TITLES = {R.string.schedule_by_date_tab, R.string.schedule_by_modality_tab};
    private static final int[] PAGES = {};//{R.layout.schedule_by_date, R.layout.schedule_by_modality};

    private final Context context;

    public ScheduleTabsAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return context.getResources().getString(PAGE_TITLES[position]);
    }

    @Override
    public Fragment getItem(int position) {
        int layout = PAGES[position];

        SimpleCustomFragment fragment = new SimpleCustomFragment();
        Bundle args = new Bundle();
        args.putInt(SimpleCustomFragment.FRAGMENT_LAYOUT, layout);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public int getCount() {
        return NUMBER_OF_PAGES;
    }

}
