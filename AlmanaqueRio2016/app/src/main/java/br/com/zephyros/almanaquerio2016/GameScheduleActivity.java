package br.com.zephyros.almanaquerio2016;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;

import br.com.zephyros.almanaquerio2016.schedule.adapter.TeamAdapter;
import br.com.zephyros.almanaquerio2016.schedule.model.GameDetails;
import br.com.zephyros.almanaquerio2016.schedule.parser.JsonLoader;
import br.com.zephyros.almanaquerio2016.util.DateFormatUtil;

public class GameScheduleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_schedule);

        Bundle b = getIntent().getExtras();
        String file = b.getString("file");
        Context context = getBaseContext();

        try {
            String json = JsonLoader.loadJSONFromAssets(file, context);
                GameDetails gameDetails = new GameDetails(new JSONObject(json));

            TextView modalityTitle = (TextView)findViewById(R.id.modality_title);
            int modality = context.getResources().getIdentifier(gameDetails.getModality(), "string", context.getPackageName());
            modalityTitle.setText(modality);

            ImageView image = (ImageView)findViewById(R.id.modality_image);
            int imageResId = context.getResources().getIdentifier(gameDetails.getImageName(), "drawable", context.getPackageName());
            image.setImageResource(imageResId);

            TextView gameDate = (TextView)findViewById(R.id.game_date);
            String gameDateAsString = DateFormatUtil.getReadableDateString(this, gameDetails.getDate());
            gameDate.setText(gameDateAsString);

            TextView gameTime = (TextView)findViewById(R.id.game_time);
            gameTime.setText(gameDetails.getTime());

            TextView gamePlace = (TextView)findViewById(R.id.game_place);
            gamePlace.setText(gameDetails.getPlace());

            if(gameDetails.getCompetitionStage() != null) {
                TextView competitionStage = (TextView) findViewById(R.id.game_competition_stage);
                int stageResId = context.getResources().getIdentifier(gameDetails.getCompetitionStage(), "string", context.getPackageName());
                competitionStage.setText(stageResId);
            }

            TextView gameChannel = (TextView)findViewById(R.id.game_channel);
            gameChannel.setText(gameDetails.getBrazilianChannel());

            TextView gameDescription = (TextView)findViewById(R.id.game_description);
            gameDescription.setText(gameDetails.getDescription());

            GridView teamGrid = (GridView) findViewById(R.id.game_teams);
            teamGrid.setAdapter(new TeamAdapter(context, gameDetails.getTeams()));


        } catch (JSONException | ParseException | IOException e) {
            e.printStackTrace();
        }

    }



}
