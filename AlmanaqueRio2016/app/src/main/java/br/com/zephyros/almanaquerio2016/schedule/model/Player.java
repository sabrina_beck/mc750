package br.com.zephyros.almanaquerio2016.schedule.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by sabrina on 21/11/15.
 */
public class Player {

    private String name;

    public Player(JSONObject player) throws JSONException {
        this.name = player.getString("name");
    }

    public String getName() {
        return name;
    }
}
