package br.com.zephyros.almanaquerio2016;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MedalDetailCardListAdapter extends ArrayAdapter<MedalDetailCard> {
    private static final String TAG = "MedalDetailCardArrayAdapter";
    private List<MedalDetailCard> cardList = new ArrayList<MedalDetailCard>();

    static class MDCardViewHolder {
        TextView modality;
        TextView date;
        TextView place;
        TextView athlete;
    }

    public MedalDetailCardListAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    @Override
    public void add(MedalDetailCard object) {
        cardList.add(object);
        super.add(object);
    }

    @Override
    public int getCount() {
        return this.cardList.size();
    }

    @Override
    public MedalDetailCard getItem(int index) {
        return this.cardList.get(index);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        MDCardViewHolder viewHolder;

        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.medal_detail_card, parent, false);
            viewHolder = new MDCardViewHolder();
            viewHolder.modality = (TextView) row.findViewById(R.id.modality);
            viewHolder.date = (TextView) row.findViewById(R.id.date);
            viewHolder.place = (TextView) row.findViewById(R.id.place);
            viewHolder.athlete = (TextView) row.findViewById(R.id.athlete);
            row.setTag(viewHolder);
        } else {
            viewHolder = (MDCardViewHolder)row.getTag();
        }
        MedalDetailCard card = getItem(position);
        viewHolder.modality.setText(card.modality);
        viewHolder.date.setText(card.date);
        viewHolder.place.setText(card.place);
        viewHolder.athlete.setText(card.athlete);
        return row;
    }

    public Bitmap decodeToBitmap(byte[] decodedByte) {
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }
}