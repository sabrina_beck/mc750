package br.com.zephyros.almanaquerio2016;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kaique on 08/11/15.
 */
public class CardListAdapter extends ArrayAdapter<MedalCard> {
    private static final String TAG = "CardArrayAdapter";
    private List<MedalCard> cardList = new ArrayList<MedalCard>();

    static class CardViewHolder {
        TextView position;
        ImageView flag;
        TextView gold;
        TextView silver;
        TextView bronze;
    }

    public CardListAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    @Override
    public void add(MedalCard object) {
        cardList.add(object);
        super.add(object);
    }

    @Override
    public int getCount() {
        return this.cardList.size();
    }

    @Override
    public MedalCard getItem(int index) {
        return this.cardList.get(index);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        CardViewHolder viewHolder;

        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.medal_card, parent, false);
            viewHolder = new CardViewHolder();
            viewHolder.position = (TextView) row.findViewById(R.id.positionText);
            viewHolder.gold = (TextView) row.findViewById(R.id.goldText);
            viewHolder.silver = (TextView) row.findViewById(R.id.silverText);
            viewHolder.bronze = (TextView) row.findViewById(R.id.bronzeText);

            viewHolder.flag = (ImageView) row.findViewById(R.id.flagImage);
            row.setTag(viewHolder);
        } else {
            viewHolder = (CardViewHolder)row.getTag();
        }
        MedalCard card = getItem(position);
        viewHolder.position.setText(card.position);
        viewHolder.gold.setText(card.gold);
        viewHolder.silver.setText(card.silver);
        viewHolder.bronze.setText(card.bronze);

        viewHolder.flag.setImageResource(card.flag);

        return row;
    }

    public Bitmap decodeToBitmap(byte[] decodedByte) {
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }
}