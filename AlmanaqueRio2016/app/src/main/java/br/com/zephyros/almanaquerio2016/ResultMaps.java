package br.com.zephyros.almanaquerio2016;

import java.util.ArrayList;

/**
 * Created by kaique on 20/11/15.
 */
public class ResultMaps {

    private ArrayList placeId;
    private static ResultMaps instance;

    private ResultMaps() {

    }

    public static ResultMaps getInstance() {
        if (instance != null) {
            return instance;
        }
        instance = new ResultMaps();
        return instance;
    }


    public ArrayList getPlaceId() {
        return placeId;
    }

    public void setPlaceId(ArrayList placeId) {
        this.placeId = placeId;
    }
}
