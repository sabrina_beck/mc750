package br.com.zephyros.almanaquerio2016.schedule.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.zephyros.almanaquerio2016.GameScheduleActivity;
import br.com.zephyros.almanaquerio2016.MainActivity;
import br.com.zephyros.almanaquerio2016.R;
import br.com.zephyros.almanaquerio2016.schedule.model.GameSchedule;

/**
 * Created by sabrina on 20/11/15.
 */
public class ScheduleAdapter extends BaseAdapter {

    private Activity activity;
    private Context context;
    private List<GameSchedule> jogos;
    private int[] colors = new int[] {
            R.color.olympicBlack,
            R.color.olympicBlue,
            R.color.olympicGreen,
            R.color.olympicRed,
            R.color.olympicYellow
    };

    public ScheduleAdapter(Activity activity, List<GameSchedule> jogos) {
        this.activity = activity;
        this.context = activity.getBaseContext();
        this.jogos = jogos;
    }

    @Override
    public int getCount() {
        return jogos.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final GameSchedule gameSchedule = jogos.get(position);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View card = inflater.inflate(R.layout.modality_card, null);

        ImageView icon = (ImageView) card.findViewById(R.id.gamePictogram);
        int pictogram = context.getResources().getIdentifier(gameSchedule.getPictogram(), "drawable", context.getPackageName());
        icon.setImageResource(pictogram);

        TextView title = (TextView) card.findViewById(R.id.gameTitle);
        int modality = context.getResources().getIdentifier(gameSchedule.getModality(), "string", context.getPackageName());
        title.setText(modality);

        TextView hour = (TextView) card.findViewById(R.id.gameHour);
        hour.setText(gameSchedule.getHour());

        CardView cardView = (CardView) card.findViewById(R.id.modalityCard);
        cardView.setBackgroundColor(ContextCompat.getColor(context, colors[position % colors.length]));
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(card.getContext(), GameScheduleActivity.class);
                Bundle b = new Bundle();
                b.putString("file", gameSchedule.getDetailsFile());
                intent.putExtras(b);
                card.getContext().startActivity(intent);
            }
        });

        return card;
    }
}
