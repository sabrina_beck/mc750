package br.com.zephyros.almanaquerio2016.schedule.parser;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by sabrina on 20/11/15.
 */
public abstract class JsonLoader {

    public static String loadJSONFromAssets(String filename, Context context) throws IOException {
        String json = null;
        InputStream is = context.getAssets().open(filename);
        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();
        json = new String(buffer, "UTF-8");
        return json;

    }

}
