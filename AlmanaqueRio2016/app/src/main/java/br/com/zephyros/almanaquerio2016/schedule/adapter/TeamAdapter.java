package br.com.zephyros.almanaquerio2016.schedule.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.zephyros.almanaquerio2016.R;
import br.com.zephyros.almanaquerio2016.schedule.model.Team;

/**
 * Created by sabrina on 21/11/15.
 */
public class TeamAdapter extends BaseAdapter {
    private Context context;
    private List<Team> teams;

    public TeamAdapter(Context context, List<Team> teams) {
        this.context = context;
        this.teams = teams;
    }

    @Override
    public int getCount() {
        return teams.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Team team = teams.get(position);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View teamView = inflater.inflate(R.layout.game_schedule_team, null);

        ImageView countryIcon = (ImageView) teamView.findViewById(R.id.team_country_icon);
        int countryIconResId = context.getResources().getIdentifier(team.getCountryIcon(), "drawable", context.getPackageName());
        countryIcon.setImageResource(countryIconResId);

        TextView country = (TextView) teamView.findViewById(R.id.team_country);
        int countryResId = context.getResources().getIdentifier(team.getCountry(), "string", context.getPackageName());
        country.setText(countryResId);

        GridView players = (GridView)teamView.findViewById(R.id.team_players);
        players.setAdapter(new PlayerAdapter(context, team.getPlayers()));

        return teamView;
    }
}
