package br.com.zephyros.almanaquerio2016.schedule.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sabrina on 21/11/15.
 */
public class Team {

    private String country;
    private String countryIcon;
    private List<Player> players;

    public Team(JSONObject team) throws JSONException {
        this.country = team.getString("country");
        this.countryIcon = team.getString("country_icon");

        JSONArray players = team.getJSONArray("players");
        this.players = new ArrayList<>();
        for(int i = 0; i < players.length(); i++) {
            Player player = new Player(players.getJSONObject(i));
            this.players.add(player);
        }
    }

    public String getCountry() {
        return country;
    }

    public String getCountryIcon() {
        return countryIcon;
    }

    public List<Player> getPlayers() {
        return players;
    }
}
