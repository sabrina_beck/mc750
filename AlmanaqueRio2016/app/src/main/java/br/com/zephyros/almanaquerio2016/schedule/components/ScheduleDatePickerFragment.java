package br.com.zephyros.almanaquerio2016.schedule.components;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.DatePicker;
import android.widget.GridView;
import android.widget.TextView;

import java.io.IOException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.zephyros.almanaquerio2016.ScheduleActivity;
import br.com.zephyros.almanaquerio2016.R;
import br.com.zephyros.almanaquerio2016.schedule.adapter.ScheduleAdapter;
import br.com.zephyros.almanaquerio2016.schedule.model.GameSchedule;
import br.com.zephyros.almanaquerio2016.util.DateFormatUtil;

/**
 * Created by sabrina on 21/11/15.
 */
public class ScheduleDatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    public static final int OLYMPIC_YEAR = 2016;
    public static final int OLYMPIC_MONTH = 8;
    public static final int OLYMPIC_FIRST_DAY = 6;
    public static final int OLYMPIC_LAST_DAY = 21;

    private final Calendar firstDate;
    private final Calendar lastDate;
    private Calendar defaultDate;

    public ScheduleDatePickerFragment() {
        firstDate = Calendar.getInstance();
        firstDate.set(OLYMPIC_YEAR, OLYMPIC_MONTH - 1, OLYMPIC_FIRST_DAY);

        lastDate = Calendar.getInstance();
        lastDate.set(OLYMPIC_YEAR, OLYMPIC_MONTH - 1, OLYMPIC_LAST_DAY);

        defaultDate = Calendar.getInstance();
        if(defaultDate.before(firstDate)) {
            defaultDate = firstDate;
        } else if(defaultDate.after(lastDate)) {
            defaultDate = lastDate;
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int defaultYear = defaultDate.get(Calendar.YEAR);
        int defaultMonth = defaultDate.get(Calendar.MONTH);
        int defaultDay = defaultDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, defaultYear, defaultMonth, defaultDay);
        dialog.getDatePicker().setMinDate(firstDate.getTimeInMillis());
        dialog.getDatePicker().setMaxDate(lastDate.getTimeInMillis());
        dialog.getDatePicker().setCalendarViewShown(false);

        return dialog;
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
        Calendar choosedCalendar = Calendar.getInstance();
        choosedCalendar.set(year, monthOfYear, dayOfMonth);
        int dayOfWeek = choosedCalendar.get(Calendar.DAY_OF_WEEK);

        addSelectedDateLabel(getActivity(), year, monthOfYear, dayOfMonth, dayOfWeek);
        updateScheduleGrid((ScheduleActivity) getActivity(), dateAsString(choosedCalendar));

    }

    public void onParentActivityCreated(Activity activity) {
        int year = defaultDate.get(Calendar.YEAR);
        int monthOfYear = defaultDate.get(Calendar.MONTH);
        int dayOfMonth = defaultDate.get(Calendar.DAY_OF_MONTH);
        int dayOfWeek = defaultDate.get(Calendar.DAY_OF_WEEK);

        addSelectedDateLabel(activity, year, monthOfYear, dayOfMonth, dayOfWeek);
        updateScheduleGrid((ScheduleActivity) activity, dateAsString(defaultDate));
    }

    @NonNull
    private String dateAsString(Calendar calendar) {
        SimpleDateFormat formatter = new SimpleDateFormat(DateFormatUtil.DEFAULT_DATE_FORMAT);
        return formatter.format(calendar.getTime());
    }

    private void updateScheduleGrid(ScheduleActivity activity, String date) {
        List<GameSchedule> displayedGames = new ArrayList<>();
        try {
            List<GameSchedule> gamesSchedule = GameSchedule.getGamesSchedule(activity.getBaseContext());
            for(GameSchedule gameSchedule : gamesSchedule) {
                if(gameSchedule.getDate().equals(date)) {
                    displayedGames.add(gameSchedule);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        GridView gridView = (GridView) activity.findViewById(R.id.schedule_grid);
        ScheduleAdapter adapter = new ScheduleAdapter(activity, displayedGames);
        gridView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        gridView.invalidateViews();
    }

    private void addSelectedDateLabel(Activity activity, int year, int monthOfYear, int dayOfMonth, int dayOfWeek) {
        String formatedDate = DateFormatUtil.getReadableDateString(activity, year, monthOfYear, dayOfMonth, dayOfWeek);

        TextView textView = (TextView) activity.findViewById(R.id.scheduleDateText);
        textView.setText(formatedDate);
    }

}
