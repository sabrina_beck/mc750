package br.com.zephyros.almanaquerio2016;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;

public class MedalDetailActivity extends AppCompatActivity {

    private MedalDetailCardListAdapter goldAdapter;
    private MedalDetailCardListAdapter silverAdapter;
    private MedalDetailCardListAdapter bronzeAdapter;
    private ListView listViewGold;
    private ListView listViewSilver;
    private ListView listViewBronze;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medal_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Gold
//        listViewGold = (ListView) findViewById(R.id.detail_listView_gold);
//
//        listViewGold.addHeaderView(new View(this));
//        listViewGold.addFooterView(new View(this));
//
//        goldAdapter = new MedalDetailCardListAdapter(getApplicationContext(), R.layout.content_medal_detail);
//
//        goldAdapter.add(new MedalDetailCard("Judô", "11 de Agosto", "Arena Carioca 2", "Mayra Aguiar"));
//        goldAdapter.add(new MedalDetailCard("Natação", "13 de Agosto", "Estádio Aquático Olímpico", "Bruno Fratus"));
//        goldAdapter.add(new MedalDetailCard("Tiro com arco", "12 de Agosto", "Sambódromo", "Marcus Vinícius D'Almeida"));
//        goldAdapter.add(new MedalDetailCard("Vôlei de praia", "18 de Agosto", "Arena de Vôlei de Praia", "Equipe: Ricardo e Emanuel"));
//        goldAdapter.add(new MedalDetailCard("Handebol Feminino", "20 de Agosto", "Arena do Futuro", "Seleção"));
//
//        listViewGold.setAdapter(goldAdapter);
//
//        //Silver
//        listViewSilver = (ListView) findViewById(R.id.detail_listView_silver);
//
//        listViewSilver.addHeaderView(new View(this));
//        listViewSilver.addFooterView(new View(this));
//
//        silverAdapter = new MedalDetailCardListAdapter(getApplicationContext(), R.layout.content_medal_detail);
//
//        silverAdapter.add(new MedalDetailCard("Tênis", "14 de Agosto", "Centro Olímpico de Tênis - Quadra Central", "Marcelo Melo"));
//        silverAdapter.add(new MedalDetailCard("Atletismo", "20 de Agosto", "Estádio Olímpico", "Fabiana Murer"));
//        silverAdapter.add(new MedalDetailCard("Vela", "15 de Agosto", "Marina da Glória", "Martine Grael"));
//
//        listViewSilver.setAdapter(silverAdapter);
//
//        //Bronze
//        listViewBronze = (ListView) findViewById(R.id.detail_listView_bronze);
//
//        listViewBronze.addHeaderView(new View(this));
//        listViewBronze.addFooterView(new View(this));
//
//        bronzeAdapter = new MedalDetailCardListAdapter(getApplicationContext(), R.layout.content_medal_detail);
//
//        bronzeAdapter.add(new MedalDetailCard("Ginastica artística", "16 de Agosto", "Arena Olímpica do Rio", "Arthur Zanetti"));
//        bronzeAdapter.add(new MedalDetailCard("Canoagem", "20 de Agosto", "Estádio da Lagoa", " Isaquias Queiroz  "));
//        bronzeAdapter.add(new MedalDetailCard("Judô", "06 de Agosto", "Arena Carioca 2", "Sarah Menezes"));
//        bronzeAdapter.add(new MedalDetailCard("Natação", "06 de Agosto", "Estádio Aquático Olímpico", "Felipe França"));
//
//        listViewBronze.setAdapter(bronzeAdapter);
    }

}
